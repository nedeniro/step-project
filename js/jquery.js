$(function () {
    var selectedClass = "";
    $(".new-p").click(function () {
        selectedClass = $(this).attr("data-rel");
        $("#portfolio").fadeTo(100, 0.1);
        $("#portfolio div").not("." + selectedClass).fadeOut();
        setTimeout(function () {
            $("." + selectedClass).fadeIn();
            $("#portfolio").fadeTo(200, 1);
        }, 200);
    });
});

$(document).ready(function () {
    var list = $(".tile");
    var numToShow = 12;
    var button = $("#loadMore");
    var numInList = list.length;
    list.hide();
    if (numInList > numToShow) {
        button.show();
    }
    list.slice(0, numToShow).show();

    button.click(function () {
        var showing = list.filter(':visible').length;
        list.slice(showing - 1, showing + numToShow).fadeIn();
        var nowShowing = list.filter(':visible').length;
        if (nowShowing >= numInList) {
            button.hide();
        }
    });
});
